====================
Sale Office Scenario
====================

Imports::

    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company


Install office_product::

    >>> config = activate_modules(['sale_office', 'office_product'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Get user::


    >>> User = Model.get('res.user')
    >>> admin = User(config.user)


Create customer::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()


Create offices::

    >>> Office = Model.get('company.office')
    >>> office1 = Office()
    >>> office1.name = 'Office 1'
    >>> office1.company = company
    >>> office1.save()
    >>> office2 = Office()
    >>> office2.name = 'Office 2'
    >>> office2.company = company
    >>> office2.save()


Add offices to admin user::

    >>> admin.offices.append(office1)
    >>> admin.offices.append(office2)
    >>> admin.save()
    >>> office1 = Office(office1.id)
    >>> office2 = Office(office2.id)


Create Product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.list_price = Decimal('40')
    >>> template.salable = True
    >>> template.offices.append(office1)
    >>> template.save()
    >>> product, = template.products


Create Sale::

    >>> Sale = Model.get('sale.sale')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.office = office2
    >>> sline = sale.lines.new()
    >>> sline.product = product
    >>> sline.quantity = 2.0
    >>> sale.save()
    Traceback (most recent call last):
    ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Product" in "Sale Line" is not valid according to its domain. - 

    >>> office2 = Office(office2.id)
    >>> template.offices.append(office2)
    >>> template.save()
    >>> sale.save()


Add offices to product::

    >>> office1 = Office(office1.id)
    >>> office2 = Office(office2.id)
    >>> template.offices.remove(office2)
    >>> template.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelsql.ForeignKeyError: The records could not be deleted because they are used by field "Office" of "User - Company branch office". - 