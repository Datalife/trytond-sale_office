# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta

__all__ = ['PartyOffice']


class PartyOffice(metaclass=PoolMeta):
    __name__ = 'party.party-company.office'

    @classmethod
    def _get_models_to_check(cls):
        return super()._get_models_to_check() + [
            ('sale.sale', 'office', 'party'),
            ('sale.sale', 'office', 'shipment_party'),
        ]
