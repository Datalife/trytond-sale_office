# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import sale
from . import cost
from . import party
from . import product


def register():
    Pool.register(
        party.PartyOffice,
        sale.Sale,
        sale.SaleLine,
        module='sale_office', type_='model')
    Pool.register(
        sale.Sale2,
        sale.SaleLine2,
        module='sale_office', type_='model',
        depends=['account_office'])
    Pool.register(
        cost.SaleCost,
        module='sale_office', type_='model',
        depends=[
            'sale_cost_apply_invoice',
            'account_office'
        ])
    Pool.register(
        product.TemplateOffice,
        sale.SaleLine4,
        module='sale_office', type_='model',
        depends=['office_product'])
    Pool.register(
        sale.Sale3,
        module='sale_office', type_='model',
        depends=['office_sequence'])
    Pool.register(
        sale.SaleLine3,
        module='sale_office', type_='model',
        depends=['office_account_product', 'account_office'])
    Pool.register(
        cost.SaleCost2,
        module='sale_office', type_='model',
        depends=[
            'office_account_product',
            'account_office',
            'sale_cost_apply_invoice'
        ])
    Pool.register(
        cost.SaleCost3,
        sale.Sale4,
        module='sale_office', type_='model',
        depends=['sale_cost'])
